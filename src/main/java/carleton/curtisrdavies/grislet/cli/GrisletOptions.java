package carleton.curtisrdavies.grislet.cli;

public interface GrisletOptions {

    String getHost();

    Integer getPort();

    String getTeam();
}

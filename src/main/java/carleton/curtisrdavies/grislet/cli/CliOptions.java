package carleton.curtisrdavies.grislet.cli;

import org.apache.commons.cli.*;

public class CliOptions implements GrisletOptions {
    private final String hostname;
    private final Integer port;
    private final String team;

    public CliOptions(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(getOptions(), args);
        this.hostname = line.hasOption("host") ? line.getOptionValue(
                "host") : "";
        this.port = line.hasOption("port") ? Integer.parseInt(
                line.getOptionValue("port")) : 6000;
        this.team = line.hasOption("team") ? line.getOptionValue(
                "team") : "Grislet";
    }

    private static Options getOptions() {
        Options options = new Options();

        options.addOption(Option.builder("host")
                .argName("host")
                .longOpt("host")
                .desc("Hostname to connect to")
                .type(String.class)
                .hasArg()
                .build());

        options.addOption(Option.builder("port")
                .argName("port")
                .longOpt("port")
                .desc("Port to connect to")
                .type(Integer.class)
                .hasArg()
                .build());

        options.addOption(Option.builder("team")
                .argName("team")
                .longOpt("team")
                .desc("Team name to play as")
                .type(String.class)
                .hasArg()
                .build());

        return options;
    }

    @Override
    public String getHost() {
        return this.hostname;
    }

    @Override
    public Integer getPort() {
        return this.port;
    }

    @Override
    public String getTeam() {
        return this.team;
    }
}

package carleton.curtisrdavies.grislet.network;

import carleton.curtisrdavies.grislet.cli.GrisletOptions;
import carleton.curtisrdavies.grislet.game.GameState;
import carleton.curtisrdavies.grislet.player.PlayerInfo;
import carleton.curtisrdavies.grislet.player.Side;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NetworkClient {
    private static final int PACKET_SIZE = 4096;
    private final DatagramSocket socket;
    private final GrisletOptions options;

    public NetworkClient(GrisletOptions options) throws SocketException {
        this.options = options;
        this.socket = new DatagramSocket();
    }

    public InitInfo init() throws IOException {
        send(new NetworkMessage(MessageType.init,
                new String[]{options.getTeam(), "(version 9)"}));
        String initResponse = receive();
        System.out.println(initResponse);

        Matcher m = Pattern.compile(
                        "^\\(init\\s(\\w)\\s(\\d{1,2})\\s(\\w+?)\\).*$")
                .matcher(initResponse);
        if (!m.matches()) {
            throw new IOException(initResponse);
        }

        String sideChar = String.valueOf(m.group(1).charAt(0));
        Side side = Side.valueOf(sideChar);

        PlayerInfo initPlayer = new PlayerInfo(side, Integer.parseInt(m.group(2)));

        GameState gameState = GameState.valueOf(m.group(3));
        return new InitInfo(initPlayer, gameState);
    }

    private String receive() {
        byte[] buffer = new byte[PACKET_SIZE];
        DatagramPacket packet = new DatagramPacket(buffer, PACKET_SIZE);
        try {
            socket.receive(packet);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        return new String(buffer);
    }

    private void send(NetworkMessage message) {
        String msgString = message.toString();
        System.out.println(msgString);
        byte[] buffer = Arrays.copyOf(msgString.getBytes(), PACKET_SIZE);
        try {
            DatagramPacket packet = new DatagramPacket(buffer, PACKET_SIZE,
                    InetAddress.getByName(options.getHost()),
                    options.getPort());
            socket.send(packet);
        } catch (IOException e) {
            System.err.println("socket sending error " + e);
        }

    }
}

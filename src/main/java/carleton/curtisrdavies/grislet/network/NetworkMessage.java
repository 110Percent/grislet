package carleton.curtisrdavies.grislet.network;

public class NetworkMessage {
    private final MessageType messageType;
    private final String[] args;

    public NetworkMessage(MessageType messageType, String[] args) {
        this.messageType = messageType;
        this.args = args;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("(").append(
                messageType.toString());
        for (String arg : args) {
            sb.append(" ").append(arg);
        }
        sb.append(")");
        return sb.toString();
    }
}

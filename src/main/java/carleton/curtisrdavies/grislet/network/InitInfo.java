package carleton.curtisrdavies.grislet.network;

import carleton.curtisrdavies.grislet.game.GameState;
import carleton.curtisrdavies.grislet.player.PlayerInfo;

public class InitInfo {
    private final PlayerInfo playerInfo;
    private final GameState gameState;

    public InitInfo(PlayerInfo playerInfo, GameState gameState) {
        this.playerInfo = playerInfo;
        this.gameState = gameState;
    }

    public PlayerInfo getPlayerInfo() {
        return playerInfo;
    }

    public GameState getGameState() {
        return gameState;
    }
}

package carleton.curtisrdavies.grislet.player;

public class PlayerInfo {
    private final Side side;
    private final int number;

    public PlayerInfo(Side side, int number) {
        this.side = side;
        this.number = number;
    }

    public Side getSide() {
        return side;
    }

    public int getNumber() {
        return number;
    }
}

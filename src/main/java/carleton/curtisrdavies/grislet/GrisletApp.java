package carleton.curtisrdavies.grislet;

import carleton.curtisrdavies.grislet.cli.GrisletOptions;
import carleton.curtisrdavies.grislet.cli.CliOptions;
import carleton.curtisrdavies.grislet.network.InitInfo;
import carleton.curtisrdavies.grislet.network.NetworkClient;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.net.SocketException;

public class GrisletApp {
    private final GrisletOptions options;
    private final NetworkClient networkClient;

    public GrisletApp(GrisletOptions options) throws SocketException {
        this.options = options;
        this.networkClient = new NetworkClient(options);
    }

    public static void main(String[] args) throws Exception {
        GrisletOptions options = null;
        try {
            options = new CliOptions(args);
        } catch (ParseException exception) {
            System.err.println("Error parsing command line arguments");
            exception.printStackTrace();
            System.exit(1);
        }

        new GrisletApp(options).start();
    }

    public void start() {
        System.out.println("Hello " + options.getTeam() + "!");
        try {
            InitInfo initInfo = networkClient.init();
            System.out.println(initInfo.getGameState());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
